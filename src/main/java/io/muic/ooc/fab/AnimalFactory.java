package io.muic.ooc.fab;

import java.util.HashMap;
import java.util.Map;

public class AnimalFactory {


    private static Map<AnimalType, Class> animalClassMap = new HashMap<AnimalType, Class>(){{
            AnimalType[] animalTypes = AnimalType.values();
        for (AnimalType animalType : animalTypes) {
            put(animalType, animalType.getAnimalClass());
        }
    }};


    public static Animal createAnimal(AnimalType animalType, boolean randomAge, Field field, Location location){
        Class animalClass = animalClassMap.get(animalType);
        return createAnimal(animalClass, randomAge, field, location);
    }

    public static Animal createAnimal(Class animalClass, boolean randomAge, Field field, Location location){
        if(animalClass != null){
            try{
                Animal animal = (Animal) animalClass.newInstance();
                animal.initialize(randomAge, field, location);
                return animal;
            } catch (InstantiationException | IllegalAccessException e){
                e.printStackTrace();
            }
        }
        throw new IllegalArgumentException("Unknown animalType");
    }

}
