package io.muic.ooc.fab;

import java.util.Iterator;
import java.util.List;

public abstract class Predator extends Animal{


    // The predator's food level, which is increased by eating prey.
    private int foodLevel;

    @Override
    public void initialize(boolean randomAge, Field field, Location location) {
        super.initialize(randomAge, field, location);
        foodLevel = RANDOM.nextInt(AnimalType.RABBIT.getFoodValue());
    }

    /**
     * This is what the animal does most of the time: it hunts for prey. In the
     * process, it might breed, die of hunger, or die of old age.
     *
     * @param newAnimal A list to return newly born predator.
     */

    @Override
    public void act(List<Animal> newAnimal) {
        incrementHunger();
        super.act(newAnimal);
    }

     private Location findFood() {
        List<Location> adjacent = field.adjacentLocations(getLocation());
        Iterator<Location> it = adjacent.iterator();
        AnimalType[] preyAnimals = getPreyAnimals();

        while (it.hasNext()) {
            Location where = it.next();
            Animal animal = (Animal) field.getObjectAt(where);
            if(animal != null) {
                for (AnimalType edibleAnimal : preyAnimals) {
                    if (animal.getClass() == edibleAnimal.getAnimalClass() && animal.isAlive()) {
                        animal.setDead();
                        foodLevel = edibleAnimal.getFoodValue();
                        return where;
                    }
                }
            }
        }

        return null;
    }

    protected Location moveToNewLocation(){
        Location newLocation = findFood();
        if (newLocation == null) {
            // No food found - try to move to a free location.
            newLocation = field.freeAdjacentLocation(getLocation());
        }
        return newLocation;
    }
    /**
     * Make this animal more hungry. This could result in the fox's death.
     */

    private void incrementHunger() {
        foodLevel--;
        if (foodLevel <= 0) {
            setDead();
        }
    }

    protected abstract AnimalType[] getPreyAnimals();

}
