package io.muic.ooc.fab;

public abstract class Prey extends Animal{

    @Override
    protected Location moveToNewLocation() {
        return field.freeAdjacentLocation(getLocation());
    }

}
