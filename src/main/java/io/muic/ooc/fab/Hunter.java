package io.muic.ooc.fab;

public class Hunter extends Predator{

    @Override
    protected void setDead() {}

    @Override
    protected AnimalType[] getPreyAnimals() {
        AnimalType[] allAnimal = AnimalType.values();
        AnimalType[] tmp = new AnimalType[allAnimal.length-1];
        for(int i = 0; i < allAnimal.length; i++){
            if(allAnimal[i] != AnimalType.HUNTER){
                tmp[i] = allAnimal[i];
            }
        }
        return tmp;
    }

    @Override
    protected int getMaxAge() {
        return 400;
    }

    @Override
    protected double getBreedingProbability() {
        return 0.003;
    }

    @Override
    protected int getMaxLitterSize() {
        return 1;
    }

    @Override
    protected int getBreedingAge() {
        return 25;
    }
}
